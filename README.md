# Visages Animés v6.0

Correction du TP JavaScript '[les Visages Animés](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html)' (TP 8)

Ce repository git permet de suivre l'évolution du programme **Visages Animés** exercice par exercice.
A chaque version est associée un tag (**v1.x**, **v2.x**, ... le numéro de version indiquant numéro de l'exercice traité)
qui vous permet de récupérer facilement le code correspondant à l'état de l'application à la fin de chaque 
exercice (un tag permet d'identifier facilement un commit donné).

Pour avoir le détail des différentes versions utilisez la commande

```bash
git tag -l -n
```

Pour récupérer le code source correspondant à une version donnée 

```bash
git checkout -b exercice<noversion> v<noversion>
```

qui permet de créer une branche de nom `exercice<noversion>` en récupérant le code de la version `v<noversion>`. Par exemple

```bash
git checkout -b exercice1 v1.0
```

vous permet de récupérer dans la branche `exercice1` le code de la version `v1`. **Attention** une fois cette commande effectuée, vous êtes positionné sur la branche `exercice1` (votre répertoire de travail (*working directory*) contient les fichiers correspondant au commit identifié par le tag `v1`). Vous pouvez revenir sur la branche `master` à l'aide de la commande

```
git checkout master
```

Vous pouvez alors (si vous le souhaitez) détruire la branche `exercice1` par la commande 

```bash
git branch -d exercice1
```

Vous pouvez aussi à l'aide de la commande `git diff` visualiser facilement les différences entre deux versions d'un code. Par exemple

```
git diff v3.0 v4.0  ./js/Visage.js
```
vous montrera les différences entre la version `v4.0` et la version `v3.0` du code JavaScript `Visage.js`

## Les différentes versions

Les différentes versions de l'application vous sont présentées ici dans l'ordre décroissant, c'est à dire
de la dernière version (qui correspond au dernier exercice traité) à la première version (qui correspond au premier exercice). 

###  **Exercice 6 : Démarrer/arrêter l'animation avec la souris (tag v6.0)**

Solution de l'[exercice 6](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section06) du TP 8.

Dans ce sixième exercice on rajoute la possibilité démarrer/arrêter l'animation par un click sur le canvas.

###  **Exercice 5 : Génération aléatoire de 1 à 10 visages (tag v5.0)**

Solution de l'[exercice 5](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section05) du TP 8.

Dans ce cinquième exercice on rajoute la possibilité de choisir le nombre de visages à animer (entre 1 et 10), et de générer autant de visages de manière aléatoire.

### **Exercice 4 : Modifier la couleur de tous les visages (tag v4.0)**

Solution de l'[exercice 4](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section04) du TP 8.

Dans ce quatrième exercice on rajoute la possibilité de modifier la couleur de **tous** les visages à l'aide d'un input de type <code>color</code>.

### **Exercice 3 : Contrôle de l'animation (tag v3.0)**

Solution de l'[exercice 3](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section03) du TP 8.

Dans ce troisième exercice on rajoute la possibilité de démarrer ou d'arrêter l'animation à l'aide de deux boutons (Start et Stop).

### **Exercice 2 : Animer les visages (tag v2.0)**

Solution de l'[exercice 2](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section02) du TP 8.

Dans ce deuxième exercice on dote les visages de la possibilité de se déplacer dans le canvas  (en rebondissant sur les bords) 
et on modifie le programme principal de manière à ce que l'animation des deux visages soit lancée au chargement de la page.
### **Exercice 1 : Créer et afficher deux visages (tag v1.0)**

Solution de l'[exercice 1](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp08_JSObjects1/index.html#section01) du TP 8.

Dans ce premier exercice on se contente de créer deux visages et de les afficher dans le canvas sans les faire se déplacer.
